import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/guards/auth.guard';
import { RegistrationGuard } from './services/guards/registration.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./page/home/home.module').then( m => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'verify-email',
    loadChildren: () => import('./page/authentication/verify-email/verify-email.module').then( m => m.VerifyEmailPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./page/authentication/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./page/authentication/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'navigations',
    loadChildren: () => import('./page/navigations/navigations.module').then( m => m.NavigationsPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'registrationAdmin',
    loadChildren: () => import('./page/registration/registration-admin/registration-admin.module')
    .then( m => m.RegistrationAdminPageModule),
    canActivate: [RegistrationGuard]
  },
  {
    path: 'registrationCustomer',
    loadChildren: () => import('./page/registration/registration-customer/registration-customer.module')
    .then( m => m.RegistrationCustomerPageModule),
  },
  {
    path: 'registrationRestaurant',
    loadChildren: () => import('./page/registration/registration-restaurant/registration-restaurant.module')
    .then( m => m.RegistrationRestaurantPageModule),
    canActivate: [RegistrationGuard]
  },
  {
    path: 'registrationStaff',
    loadChildren: () => import('./page/registration/registration-staff/registration-staff.module')
    .then( m => m.RegistrationStaffPageModule),
    canActivate: [RegistrationGuard]
  },
  {
    path: 'createAdmin',
    loadChildren: () => import('./page/create/create-admin/create-admin.module').then( m => m.CreateAdminPageModule),
    canActivate: [RegistrationGuard]
  },
  {
    path: 'createCustomer',
    loadChildren: () => import('./page/create/create-customer/create-customer.module').then( m => m.CreateCustomerPageModule),
  },
  {
    path: 'createRestaurant',
    loadChildren: () => import('./page/create/create-restaurant/create-restaurant.module').then( m => m.CreateRestaurantPageModule),
    canActivate: [RegistrationGuard]
  },
  {
    path: 'createStaff',
    loadChildren: () => import('./page/create/create-staff/create-staff.module').then( m => m.CreateStaffPageModule),
    canActivate: [RegistrationGuard]
  },
  {
    path: 'listAdmin',
    loadChildren: () => import('./page/list/list-admin/list-admin.module').then( m => m.ListAdminPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'listCustomer',
    loadChildren: () => import('./page/list/list-customer/list-customer.module').then( m => m.ListCustomerPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'listRestaurant',
    loadChildren: () => import('./page/list/list-restaurant/list-restaurant.module').then( m => m.ListRestaurantPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'listStaff',
    loadChildren: () => import('./page/list/list-staff/list-staff.module').then( m => m.ListStaffPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'editAdmin/:id',
    loadChildren: () => import('./page/edit/edit-admin/edit-admin.module').then( m => m.EditAdminPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'editCustomer/:id',
    loadChildren: () => import('./page/edit/edit-customer/edit-customer.module').then( m => m.EditCustomerPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'editRestaurant/:id',
    loadChildren: () => import('./page/edit/edit-restaurant/edit-restaurant.module').then( m => m.EditRestaurantPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'editStaff/:id',
    loadChildren: () => import('./page/edit/edit-staff/edit-staff.module').then( m => m.EditStaffPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'listTurn',
    loadChildren: () => import('./page/list/list-turn/list-turn.module').then( m => m.ListTurnPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'createTurn',
    loadChildren: () => import('./page/create/create-turn/create-turn.module').then( m => m.CreateTurnPageModule),
    canActivate: [RegistrationGuard]
  },
  {
    path: 'editTurn/:id',
    loadChildren: () => import('./page/edit/edit-turn/edit-turn.module').then( m => m.EditTurnPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'listBooking',
    loadChildren: () => import('./page/list/list-booking/list-booking.module').then( m => m.ListBookingPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'createBooking/:id',
    loadChildren: () => import('./page/create/create-booking/create-booking.module').then( m => m.CreateBookingPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'editBooking/:id',
    loadChildren: () => import('./page/edit/edit-booking/edit-booking.module').then( m => m.EditBookingPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'chooseRestaurant',
    loadChildren: () => import('./page/choose/choose-restaurant/choose-restaurant.module').then( m => m.ChooseRestaurantPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'chooseTurn/:id',
    loadChildren: () => import('./page/choose/choose-turn/choose-turn.module').then( m => m.ChooseTurnPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'listCheck',
    loadChildren: () => import('./page/list/list-check/list-check.module').then( m => m.ListCheckPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'forgotPassword',
    loadChildren: () => import('./page/authentication/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
