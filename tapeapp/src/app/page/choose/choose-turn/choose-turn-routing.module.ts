import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseTurnPage } from './choose-turn.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseTurnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseTurnPageRoutingModule {}
