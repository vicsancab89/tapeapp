import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseTurnPageRoutingModule } from './choose-turn-routing.module';

import { ChooseTurnPage } from './choose-turn.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseTurnPageRoutingModule
  ],
  declarations: [ChooseTurnPage]
})
export class ChooseTurnPageModule {}
