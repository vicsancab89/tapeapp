import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BookingService } from '../../../services/booking.service';
import { CustomerService } from '../../../services/customer.service';
import { TurnService } from '../../../services/turn.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-choose-turn',
  templateUrl: './choose-turn.page.html',
  styleUrls: ['./choose-turn.page.scss'],
})
export class ChooseTurnPage implements OnInit {
  public turns = [];
  public numberPersonTotal = 0;
  constructor(
    private activatedRoute: ActivatedRoute,
    private turnService: TurnService,
    private bookingService: BookingService,
    private authService: AuthenticationService,
    private customerService: CustomerService,
    public utility: UtilityService
  ) {}

  ngOnInit() {
    console.log(this.activatedRoute.snapshot.paramMap.get('id'));
    this.turnService.getTurnByRestaurantId(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((turnSnapShot) =>{
      this.turns = [];
      turnSnapShot.forEach((turnData: any) => {
        if(this.utility.compareDate(turnData.payload.doc.data().date)){
          this.turns.push({
            id: turnData.payload.doc.id,
            date: this.utility.dateFormat(turnData.payload.doc.data().date),
            hour: this.utility.timeFormat(turnData.payload.doc.data().date),
            diners: turnData.payload.doc.data().diners,
          });
        }
      });
    });
  }
  personTotal(turnId){
    this.bookingService.getBookingByTurnId(turnId).subscribe((bookingSnapShot) => {
      this.numberPersonTotal = 0;
      bookingSnapShot.forEach((bookingData: any) => {
        this.numberPersonTotal += bookingData.payload.doc.data().numberPerson;
      });
    });
  }
  maximo(numberPersonTotal: number, diners: number): number{
    return (diners - numberPersonTotal);
  }
}
