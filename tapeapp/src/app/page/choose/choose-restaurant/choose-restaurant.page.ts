import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { RestaurantService } from '../../../services/restaurant.service';

@Component({
  selector: 'app-choose-restaurant',
  templateUrl: './choose-restaurant.page.html',
  styleUrls: ['./choose-restaurant.page.scss'],
})
export class ChooseRestaurantPage implements OnInit {
  public restaurants: any[] = [];
  constructor(
    private restaurantService: RestaurantService,
    private userService: UserService) { }

  ngOnInit() {
    this.restaurantService.allRestaurant().subscribe((restaurantSnapShot: any) => {
      this.restaurants = [];
      restaurantSnapShot.forEach((restaurantData: any)=>{
      this.userService.getUser(restaurantData.payload.doc.data().uid).subscribe((userData: any) =>{
        if(userData.payload.data().isActive){
          this.restaurants.push({
            id: restaurantData.payload.doc.id,
            data: restaurantData.payload.doc.data()
          });
        }
      });
      });
    });
  }
}
