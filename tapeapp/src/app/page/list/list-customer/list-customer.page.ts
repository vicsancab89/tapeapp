import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BookingService } from '../../../services/booking.service';
import { CustomerService } from '../../../services/customer.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-list-customer',
  templateUrl: './list-customer.page.html',
  styleUrls: ['./list-customer.page.scss'],
})
export class ListCustomerPage implements OnInit {
  public filtraCustomer = '';
  customers: any = [];
  email: any = null;
  constructor(
    public customerService: CustomerService,
    public userService: UserService,
    public modalController: ModalController,
    private authService: AuthenticationService,
    private bookingService: BookingService,
    private router: Router
  ) { }

  ngOnInit() {
  this.customerService.allCustomer().subscribe((customerSnapshot) => {
      this.customers = [];
      customerSnapshot.forEach((customerData: any) =>{
        this.userService.getUser(customerData.payload.doc.data().uid).subscribe((userData: any)=>{
          this.email = userData.payload.data().email;
          if(userData.payload.data().isActive){
            this.customers.push({
              id: customerData.payload.doc.id,
              data: customerData.payload.doc.data(),
              user: userData.payload.data(),
              email: this.email
            });
          }
        });
      });
    });
  }
  onRoute(){
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['listBooking']);
  }
  delete(user, customer){
    this.authService.delete(user);
    this.bookingService.getBookingByCustomerId(customer.id).subscribe((bookingSnapShot: any) => {
      bookingSnapShot.forEach((bookingData: any) =>{
        this.bookingService.deleteBooking(bookingData.payload.doc.id);
      });
    });
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['navigations']);
  }
}
