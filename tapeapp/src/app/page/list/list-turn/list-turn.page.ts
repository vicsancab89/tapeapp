import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Booking } from '../../../models/booking';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BookingService } from '../../../services/booking.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { TurnService } from '../../../services/turn.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-list-turn',
  templateUrl: './list-turn.page.html',
  styleUrls: ['./list-turn.page.scss'],
})
export class ListTurnPage implements OnInit {
 turns: any[] = [];
 public filtraTurn = '';
  constructor(
    public modalController: ModalController,
    private turnService: TurnService,
    private restaurantService: RestaurantService,
    private authService: AuthenticationService,
    private bookingService: BookingService,
    private utility: UtilityService,
    private router: Router
  ) { }

  ngOnInit() {
    this.restaurantService.getRestaurantByUid(this.authService.userLogado.uid).subscribe((restaurantSnapShot: any) =>{
      restaurantSnapShot.forEach((restaurantData: any) => {
        console.log(restaurantData.payload.doc.id);
          this.turnService.getTurnByRestaurantId(restaurantData.payload.doc.id).subscribe((turnSnapShot) => {
            this.turns = [];
            turnSnapShot.forEach((turnData: any) => {
              if(this.utility.compareDate(turnData.payload.doc.data().date)){
                this.turns.push({
                  id: turnData.payload.doc.id,
                  date: this.utility.dateFormat(turnData.payload.doc.data().date),
                  hour: this.utility.timeFormat(turnData.payload.doc.data().date),
                  numberBooking: turnData.payload.doc.data().numberBooking,
                  diners: turnData.payload.doc.data().diners,
                  restaurantId: turnData.payload.doc.data().restaurantId
                });
              }
              });
          });
      });
    });
  }
  onRoute(){
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['navigations']);
  }
  delete(turn){
    this.bookingService.getBookingByTurnId(turn.id).subscribe((bookingSnapShot: any) => {
      bookingSnapShot.forEach((bookingData: any) => {
        this.bookingService.deleteBooking(bookingData.payload.doc.id);
      });
    this.turnService.deleteTurn(turn.id);
    });
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['listTurn']);
  }
}
