import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListTurnPage } from './list-turn.page';

const routes: Routes = [
  {
    path: '',
    component: ListTurnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListTurnPageRoutingModule {}
