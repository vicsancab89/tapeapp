import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListTurnPageRoutingModule } from './list-turn-routing.module';

import { ListTurnPage } from './list-turn.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListTurnPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [ListTurnPage]
})
export class ListTurnPageModule {}
