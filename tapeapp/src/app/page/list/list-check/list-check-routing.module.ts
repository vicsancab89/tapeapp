import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListCheckPage } from './list-check.page';

const routes: Routes = [
  {
    path: '',
    component: ListCheckPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListCheckPageRoutingModule {}
