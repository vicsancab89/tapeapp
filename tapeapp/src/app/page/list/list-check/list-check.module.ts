import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ListCheckPageRoutingModule } from './list-check-routing.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ListCheckPage } from './list-check.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    ListCheckPageRoutingModule,
  ],
  declarations: [ListCheckPage]
})
export class ListCheckPageModule {}
