import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Check } from '../../../models/check';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { CheckService } from '../../../services/check.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { StaffService } from '../../../services/staff.service';
import { UserService } from '../../../services/user.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-list-check',
  templateUrl: './list-check.page.html',
  styleUrls: ['./list-check.page.scss'],
})
export class ListCheckPage implements OnInit {
  today = new Date (Date.now()).toISOString();
  restaurants = [];
  staffId = '';
  restaurantId = '';
  staffs = [];
  checks = [];
  userAccount: any = {};
  filtraCheck = '';
  constructor(
    private authService: AuthenticationService,
    private restaurantService: RestaurantService,
    private checkService: CheckService,
    private staffService: StaffService,
    public utility: UtilityService,
    public modalController: ModalController,
    private router: Router,
    private userService: UserService
  ) {
    console.log(this.authService.userLogado.uid);
    this.userService.getUser(this.authService.userLogado.uid).subscribe((userData: any) =>{
      this.userAccount = userData.payload.data();
    switch (userData.payload.data().roleId) {
      case 'restaurant':
        this.restaurantService.getRestaurantByUid(this.authService.userLogado.uid).subscribe((restaurantSnapShot) =>{
          this.restaurants = [];
          restaurantSnapShot.forEach((restaurantData: any) => {
            this.restaurants.push({
              id: restaurantData.payload.doc.id,
              data: restaurantData.payload.doc.data()
            });
            this.checkService.getCheckByRestaurantId(restaurantData.payload.doc.id).subscribe((checkSnapShot: any) =>{
              this.checks = [];
              checkSnapShot.forEach((checkData: any) =>{
                this.staffService.getStaff(checkData.payload.doc.data().staffId).subscribe((staffData: any) =>{
                  this.checks.push({
                    staff: staffData.payload.data(),
                    id: checkData.payload.doc.id,
                    data: checkData.payload.doc.data()
                  });
                });
              });
            });
          });
        });
        break;
      case 'staff':
        this.staffService.getStaffByUid(this.authService.userLogado.uid).subscribe((staffSnapShot: any) => {
          this.staffs = [];
          staffSnapShot.forEach((staffData: any) => {
            this.staffId = staffData.payload.doc.id;
            this.restaurantId = staffData.payload.doc.data().contract;
            this.staffs.push({
              id: staffData.payload.doc.id,
              data: staffData.payload.doc.data()
            });
          this.checkService.getCheckByStaffId(staffData.payload.doc.id).subscribe((checkSnapShot: any) =>{
            this.checks = [];
            checkSnapShot.forEach((checkData: any) =>{
              this.restaurantService.getRestaurant(checkData.payload.doc.data().restaurantId).subscribe((restaurantData: any) => {
                this.checks.push({
                  restaurant: restaurantData.payload.data(),
                  id: checkData.payload.doc.id,
                  data: checkData.payload.doc.data()
                });
              });
            });
          });
        });
        });
        break;
      }
    });
  }

  ngOnInit() {
  }
  checkIn(restaurantId, staffId, start, end){
    start = new Date (Date.now()).toISOString();
    const check: Check = {
      start,
      end,
      staffId,
      restaurantId
    };
    this.checkService.createCheck(check);
    this.router.navigate(['/listCheck']);
  }
  checkOut(checkId, restaurantId, staffId, start, end){
    end = new Date (Date.now()).toISOString();
    const check: Check = {
      start,
      end,
      staffId,
      restaurantId
    };
    this.checkService.updateCheck(checkId, check);
    this.router.navigate(['/listCheck']);
  }
  delete(check){
    this.checkService.deleteCheck(check.id);
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['navigations']);
  }
}
