import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BookingService } from '../../../services/booking.service';
import { CustomerService } from '../../../services/customer.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { TurnService } from '../../../services/turn.service';
import { UserService } from '../../../services/user.service';
import { UtilityService } from '../../../services/utility.service';


@Component({
  selector: 'app-list-booking',
  templateUrl: './list-booking.page.html',
  styleUrls: ['./list-booking.page.scss'],
})
export class ListBookingPage implements OnInit {
 public filtraBooking = '';
 bookings: any[] = [];
 customers = [];
 restaurants = [];
 public user: any = {};
  constructor(
    private authService: AuthenticationService,
    private restaurantService: RestaurantService,
    private bookingService: BookingService,
    private customerService: CustomerService,
    private turnService: TurnService,
    private router: Router,
    private userService: UserService,
    public modalController: ModalController,
    public utility: UtilityService
  ) {
    this.userService.getUser(this.authService.userData.uid).subscribe((userData: any) =>{
      this.user = userData.payload.data();
    });
   }

  ngOnInit() {
    this.userService.getUser(this.authService.userData.uid).subscribe((userData: any) =>{
      this.user = userData.payload.data();
      switch (userData.payload.data().roleId) {
        case 'restaurant':
          this.restaurantService.getRestaurantByUid(this.authService.userData.uid).subscribe((restaurantSnapShot: any) => {
            this.restaurants = [];
            restaurantSnapShot.forEach((restaurantData) => {
              this.restaurants.push({
                id: restaurantData.payload.doc.id,
                data: restaurantData.payload.doc.data(),
              });
              this.bookingService
                .getBookingByRestaurantId(restaurantData.payload.doc.id)
                .subscribe((bookingSnapShot: any) => {
                  this.bookings = [];
                  bookingSnapShot.forEach((bookingData) => {
                    this.customerService.getCustomer(bookingData.payload.doc.data().customerId).subscribe((customerData) => {
                      this.turnService.getTurn(bookingData.payload.doc.data().turnId).subscribe((turnData) => {
                      this.bookings.push({
                        id: bookingData.payload.doc.id,
                        data: bookingData.payload.doc.data(),
                        customer: customerData.payload.data(),
                        turn: turnData.payload.data(),
                      });
                    });
                  });
                });
              });
            });
          });
          break;
        case 'customer':
          this.customerService.getCustomerByUid(this.authService.userData.uid).subscribe((customerSnapShot: any) => {
              this.customers = [];
              customerSnapShot.forEach((customerData) => {
                this.customers.push({
                  id: customerData.payload.doc.id,
                  data: customerData.payload.doc.data(),
                });
                this.bookingService
                  .getBookingByCustomerId(customerData.payload.doc.id)
                  .subscribe((bookingSnapShot: any) => {
                    this.bookings = [];
                    bookingSnapShot.forEach((bookingData) => {
                      this.restaurantService.getRestaurant(bookingData.payload.doc.data().restaurantId).subscribe((restaurantData) => {
                        this.turnService.getTurn(bookingData.payload.doc.data().turnId).subscribe((turnData) => {
                        this.bookings.push({
                          id: bookingData.payload.doc.id,
                          data: bookingData.payload.doc.data(),
                          restaurant: restaurantData.payload.data(),
                          turn: turnData.payload.data(),
                        });
                      });
                    });
                  });
                });
              });
            });
          break;
      }
    });
  }
  onRoute(){
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['listBooking']);
  }
  delete(booking){
    this.bookingService.deleteBooking(booking.id.toString());
    this.modalController.dismiss(
    {
      dismissed: true
    });
    this.router.navigate(['navigations']);
  }
}
