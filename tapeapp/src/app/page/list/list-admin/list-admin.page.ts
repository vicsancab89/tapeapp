import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AdminService } from '../../../services/admin/admin.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-list-admin',
  templateUrl: './list-admin.page.html',
  styleUrls: ['./list-admin.page.scss'],
})
export class ListAdminPage implements OnInit {
  public filtraAdmin = '';
  admins: any = [];
  email: any = null;
  constructor(
    public adminService: AdminService,
    public userService: UserService,
    public modalController: ModalController,
    private router: Router,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
  this.adminService.allAdmin().subscribe((adminSnapshot) => {
      this.admins = [];
      adminSnapshot.forEach((adminData: any) =>{
        this.userService.getUser(adminData.payload.doc.data().uid).subscribe((userData: any)=>{
          if(userData.payload.data().isActive){
            this.email = userData.payload.data().email;
            this.admins.push({
              id: adminData.payload.doc.id,
              data: adminData.payload.doc.data(),
              email: this.email,
              user: userData.payload.data()
            });
          }
        });
      });
    });
  }
  onRoute(){
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['listAdmin']);
  }
  delete(user){
    this.authService.delete(user);
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['navigations']);
  }
}
