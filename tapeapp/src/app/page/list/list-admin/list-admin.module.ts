import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListAdminPageRoutingModule } from './list-admin-routing.module';

import { ListAdminPage } from './list-admin.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListAdminPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations:
  [
    ListAdminPage,
  ]
})
export class ListAdminPageModule {}
