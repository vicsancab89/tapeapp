import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BookingService } from '../../../services/booking.service';
import { CheckService } from '../../../services/check.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { StaffService } from '../../../services/staff.service';
import { TurnService } from '../../../services/turn.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-list-restaurant',
  templateUrl: './list-restaurant.page.html',
  styleUrls: ['./list-restaurant.page.scss'],
})
export class ListRestaurantPage implements OnInit {
  public filtraRestaurant = '';
  public restaurants: any = [];
  public email: any = null;
  constructor(
    public restaurantService: RestaurantService,
    public userService: UserService,
    public modalController: ModalController,
    private router: Router,
    private authService: AuthenticationService,
    private staffService: StaffService,
    private checkService: CheckService,
    private turnService: TurnService,
    private bookingService: BookingService
  ) { }

  ngOnInit() {
    this.restaurantService.allRestaurant().subscribe((restaurantSnapshot) => {
      this.restaurants = [];
      restaurantSnapshot.forEach((restaurantData: any) =>{
        this.userService.getUser(restaurantData.payload.doc.data().uid).subscribe((userData: any)=>{
          if(userData.payload.data().isActive){
            this.email = userData.payload.data().email;
            this.restaurants.push({
              id: restaurantData.payload.doc.id,
              data: restaurantData.payload.doc.data(),
              user: userData.payload.data(),
              email: this.email
            });
          }
        });
      });
    });
  }
  delete(user, restaurant){
    this.authService.delete(user);
    this.staffService.getStaffByContract(restaurant.id).subscribe((staffSnapShot: any) => {
      staffSnapShot.forEach((staffData: any) => {
        this.authService.delete(staffData.payload.doc.data().uid);
        this.staffService.deleteStaff(staffData.payload.doc.id);
      });
    });
    this.checkService.getCheckByRestaurantId(restaurant.id).subscribe((checkSnapShot: any) => {
      checkSnapShot.forEach((checkData: any) => {
        this.checkService.deleteCheck(checkData.payload.doc.id);
      });
    });
    this.turnService.getTurnByRestaurantId(restaurant.id).subscribe((turnSnapShot: any) => {
      turnSnapShot.forEach((turnData: any) => {
        this.turnService.deleteTurn(turnData.payload.doc.id);
      });
    });
    this.bookingService.getBookingByRestaurantId(restaurant.id).subscribe((bookingSnapShot: any) =>{
      bookingSnapShot.forEach((bookingData) =>{
        this.bookingService.deleteBooking(bookingData.payload.doc.id);
      });
    });
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['navigations']);
  }
}
