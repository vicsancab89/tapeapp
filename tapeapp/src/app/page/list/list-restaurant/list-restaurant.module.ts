import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListRestaurantPageRoutingModule } from './list-restaurant-routing.module';

import { ListRestaurantPage } from './list-restaurant.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListRestaurantPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [ListRestaurantPage]
})
export class ListRestaurantPageModule {}
