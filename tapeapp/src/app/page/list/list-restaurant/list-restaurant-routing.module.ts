import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListRestaurantPage } from './list-restaurant.page';

const routes: Routes = [
  {
    path: '',
    component: ListRestaurantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListRestaurantPageRoutingModule {}
