import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListStaffPageRoutingModule } from './list-staff-routing.module';

import { ListStaffPage } from './list-staff.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListStaffPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [ListStaffPage]
})
export class ListStaffPageModule {}
