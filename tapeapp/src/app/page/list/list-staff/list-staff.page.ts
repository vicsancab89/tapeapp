import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { userInfo } from 'os';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { CheckService } from '../../../services/check.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { StaffService } from '../../../services/staff.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-list-staff',
  templateUrl: './list-staff.page.html',
  styleUrls: ['./list-staff.page.scss'],
})
export class ListStaffPage implements OnInit {
  public filtraStaff = '';
  public restaurants: any = [];
  public staffs: any = [];
  public email: any = null;
  public roleId = '';
  user: any;
  constructor(
    public staffService: StaffService,
    public modalController: ModalController,
    public authService: AuthenticationService,
    public userService: UserService,
    public restaurantService: RestaurantService,
    private checkService: CheckService,
    private router: Router
  ) {
    this.userService.getUser(this.authService.userLogado.uid).subscribe((userData: any) => {
      this.user = userData.payload.data();
    });
  }

  ngOnInit() {
    this.userService.getUser(this.authService.userLogado.uid).subscribe((userData: any) => {
      this.user = userData.payload.data();
      this.staffService.allStaff().subscribe((staffSnapshot) => {
        this.staffs = [];
        staffSnapshot.forEach((staffData: any) => {
          this.userService.getUser(staffData.payload.doc.data().uid).subscribe((userInfo1: any) => {
              if (userData.payload.data().roleId === 'admin') {
                this.email = userInfo1.payload.data().email;
                if(userData.payload.data().isActive){
                  this.staffs.push({
                    id: staffData.payload.doc.id,
                    data: staffData.payload.doc.data(),
                    email: this.email,
                    user: userInfo1.payload.data()
                  });
                }
              } else if (userData.payload.data().roleId === 'restaurant') {
                this.restaurantService
                  .getRestaurantByUid(this.authService.userLogado.uid)
                  .subscribe((restaurantSnapshot: any) => {
                    this.restaurants = [];
                    restaurantSnapshot.forEach((restaurantData: any) => {
                      if (restaurantData.payload.doc.id
                        === staffData.payload.doc.data().contract
                      ) {
                        this.email = userInfo1.payload.data().email;
                        if(userData.payload.data().isActive){
                          this.staffs.push({
                            id: staffData.payload.doc.id,
                            data: staffData.payload.doc.data(),
                            email: this.email,
                            user: userInfo1.payload.data()
                          });
                        }
                      }
                    });
                  });
              }
            });
        });
      });
    });
  }
  onRoute(){
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['listStaff']);
  }
  delete(user, staff){
    this.authService.delete(user);
    this.checkService.getCheckByStaffId(staff.id).subscribe((checkSnapShot: any) => {
      checkSnapShot.forEach((checkData: any) =>{
        this.checkService.deleteCheck(checkData.payload.doc.id);
      });
    });
    this.modalController.dismiss(
      {
        dismissed: true
      });
    this.router.navigate(['navigations']);
  }
}
