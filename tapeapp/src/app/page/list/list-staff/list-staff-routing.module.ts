import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListStaffPage } from './list-staff.page';

const routes: Routes = [
  {
    path: '',
    component: ListStaffPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListStaffPageRoutingModule {}
