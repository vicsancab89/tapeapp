import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { StaffService } from '../../../services/staff.service';

@Component({
  selector: 'app-create-staff',
  templateUrl: './create-staff.page.html',
  styleUrls: ['./create-staff.page.scss'],
})
export class CreateStaffPage implements OnInit {
  restaurants: any[] = [];
  staffForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  validationMessages = {
    position: [{ type: 'required', message: 'Name is required.' }],
    contract: [{ type: 'required', message: 'Name is required.' }],
    name: [{ type: 'required', message: 'Name is required.' }],
    surname: [{ type: 'required', message: 'Surname is required.' }],
    card: [
      { type: 'required', message: 'DNI is required.' },
      { type: 'pattern', message: 'DNI must be example 12345678X' },
    ],
    phone: [
      { type: 'required', message: 'Phone is required' },
      { type: 'pattern', message: 'Phone must be valid' },
    ],
    adress: [{ type: 'required', message: 'Adress is required' }],
    location: [{ type: 'required', message: 'Location is required' }],
  };
  constructor(
    private staffService: StaffService,
    private router: Router,
    public authService: AuthenticationService,
    public afs: AngularFireAuth,
    public restaurantService: RestaurantService,
    public fb: FormBuilder
  ) {
    this.restaurantService.allRestaurant().subscribe((restaurantSnapshot) => {
      this.restaurants = [];
      restaurantSnapshot.forEach((restaurantData: any) => {
        console.log(this.authService.roleId);
        if (this.authService.roleId === 'admin') {
          this.restaurants.push({
            id: restaurantData.payload.doc.id,
            data: restaurantData.payload.doc.data(),
          });
        } else if (this.authService.roleId === 'restaurant') {
          if (
            this.authService.uid ===
            restaurantData.payload.doc.data().uid
          ) {
            this.restaurants.push({
              id: restaurantData.payload.doc.id,
              data: restaurantData.payload.doc.data(),
            });
          }
        }
      });
    });
  }

  ngOnInit() {
    this.staffForm = this.fb.group({
      position: new FormControl('', Validators.compose([Validators.required])),
      name: new FormControl('', Validators.compose([Validators.required])),
      surname: new FormControl('', Validators.compose([Validators.required])),
      card: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{8}[A-Z]{1}'),
        ])
      ),
      phone: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{9}'),
        ])
      ),
      adress: new FormControl('', Validators.compose([Validators.required])),
      location: new FormControl('', Validators.compose([Validators.required])),
      contract: new FormControl('', Validators.compose([Validators.required])),
      uid: this.authService.userData.uid,
    });
  }
  formSubmit() {
    if (!this.staffForm.valid) {
      return false;
    } else {
      this.staffService
        .createStaff(this.staffForm.value)
        .then((res) => {
          console.log(res);
          this.staffForm.reset();
          this.authService.sendVerificationMail('staff');
          window.alert('Send verification email');
          this.authService.userData = this.authService.userLogado;
          this.router.navigate(['/listStaff']);
        })
        .catch((error) => console.log(error));
    }
  }
}
