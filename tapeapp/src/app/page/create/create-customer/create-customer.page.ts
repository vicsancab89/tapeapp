import { DATE_PIPE_DEFAULT_TIMEZONE } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { CustomerService } from '../../../services/customer.service';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.page.html',
  styleUrls: ['./create-customer.page.scss'],
})
export class CreateCustomerPage implements OnInit {
  today: string = new Date(Date.now()).toISOString();
  customerForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  defaultDate = '1987-06-30';
  validationMessages = {
    name: [
      { type: 'required', message: 'Name is required.' },
      { type: 'maxlenght', message: 'Name max lenght is 20 character'}
    ],
    surname: [
      { type: 'required', message: 'Surname is required.' },
      { type: 'maxlenght', message: 'Surname max lenght is 80 character'}
    ],
    card: [
      { type: 'required', message: 'DNI is required.' },
      { type: 'pattern', message: 'DNI must be example 12345678X'}
    ],
    phone:[
      { type: 'required', message: 'Phone is required'},
      { type: 'pattern', message: 'Phone must be valid'}
    ],
    adress:[
      {type: 'required', message: 'Adress is required'}
    ],
    location:[
      {type: 'required', message: 'Location is required'}
    ],
    birthday:[
      {type: 'required', message: 'Birthday is required'}
    ],

  };
  constructor(
    private  customerService: CustomerService,
    private router: Router,
    public authService: AuthenticationService,
    public fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.customerForm = this.fb.group({
      name: new FormControl('', Validators.compose([
        Validators.maxLength(20),
        Validators.required
      ])),
      surname: new FormControl('', Validators.compose([
        Validators.maxLength(80),
        Validators.required,
      ])),
      card: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{8}[A-Z]{1}')
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9}')
      ])),
      adress: new FormControl('', Validators.compose([
        Validators.required
      ])),
      location: new FormControl('', Validators.compose([
        Validators.required
      ])),
      birthday: new FormControl('', Validators.compose([
        Validators.required
      ])),
      uid: this.authService.userData.uid,
    });
  }
  formSubmit() {
    if (!this.customerForm.valid) {
      return false;
    } else {
      this.customerService.createCustomer(this.customerForm.value).then(res => {
        console.log(res);
        this.customerForm.reset();
        this.authService.userData = this.authService.userLogado;
        this.router.navigate(['/verify-email']);
      })
        .catch(error => console.log(error));
    }
  }
}
