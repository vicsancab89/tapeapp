import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateTurnPage } from './create-turn.page';

const routes: Routes = [
  {
    path: '',
    component: CreateTurnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateTurnPageRoutingModule {}
