import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateTurnPageRoutingModule } from './create-turn-routing.module';

import { CreateTurnPage } from './create-turn.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CreateTurnPageRoutingModule
  ],
  declarations: [CreateTurnPage]
})
export class CreateTurnPageModule {}
