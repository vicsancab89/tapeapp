import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { TurnService } from '../../../services/turn.service';

@Component({
  selector: 'app-create-turn',
  templateUrl: './create-turn.page.html',
  styleUrls: ['./create-turn.page.scss'],
})
export class CreateTurnPage implements OnInit {
  today: string = new Date(Date.now()).toISOString();
  turnForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  restaurants: any [] = [];
  validationMessages = {
    date: [{ type: 'required', message: 'Date is required.' }],
    diners: [
      {type: 'required', message: 'Diners is required'}
    ],
    restaurantId: [{type: 'required', message: 'Restaurant is required'}]
  };
  constructor(
    private restaurantService: RestaurantService,
    private turnService: TurnService,
    public authService: AuthenticationService,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.restaurantService.getRestaurantByUid(this.authService.userData.uid).subscribe((restaurantSnapShot: any) => {
      restaurantSnapShot.forEach((restaurantData: any) => {
         this.restaurants = [];
         this.restaurants.push({
            id: restaurantData.payload.doc.id,
            data: restaurantData.payload.doc.data()
         });
      });
    });
   }

  ngOnInit() {
    this.turnForm = this.fb.group({
      date: new FormControl('', Validators.compose([Validators.required])),
      diners: new FormControl(''),
      restaurantId: new FormControl('', Validators.compose([Validators.required])),
    });
  }
  capacity(): number{
    this.restaurantService.getRestaurantByUid(this.authService.userData.uid).subscribe((restaurantSnapShot: any) => {
      restaurantSnapShot.forEach((restaurantData: any) => {
         this.restaurants = [];
         this.restaurants.push({
            capacity: restaurantData.payload.doc.data().capacity
         });
         return this.restaurants[0].capacity;
      });
    });
    return null;
  }
  formSubmit() {
    if (!this.turnForm.valid) {
      return false;
    } else {
      this.turnService.createTurn(this.turnForm.value).then(res => {
        console.log(res);
        this.turnForm.reset();
        this.router.navigate(['/listTurn']);
      })
        .catch(error => console.log(error));
    }
  }
}
