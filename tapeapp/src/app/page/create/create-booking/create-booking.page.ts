import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BookingService } from '../../../services/booking.service';
import { CustomerService } from '../../../services/customer.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { TurnService } from '../../../services/turn.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.page.html',
  styleUrls: ['./create-booking.page.scss'],
})
export class CreateBookingPage implements OnInit {
  today: string = new Date(Date.now()).toISOString();
  numberPersonTotal = 0;
  turnId = this.activatedRoute.snapshot.paramMap.get('id');
  turn = {};
  date: any;
  hour: any;
  diners = 0;
  restaurantId = '';
  restaurant: any = {};
  customers: any[] = [];
  bookingForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  validationMessages = {
    numberPerson: [
      { type: 'required', message: 'Number person is required.' }
    ],
    restaurantId: [
      { type: 'required', message: 'Restaurant is required.' },
    ],
    customerId:[
      { type: 'required', message: 'Customer is required'},
    ],
    turnId:[
      {type: 'required', message: 'Adress is required'}
    ],
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private turnService: TurnService,
    private customerService: CustomerService,
    private restaurantService: RestaurantService,
    private bookingService: BookingService,
    private fb: FormBuilder,
    private router: Router,
    private utility: UtilityService
  ) {
      this.turnService.getTurn(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((turnData: any) => {
        this.restaurantService.getRestaurant(turnData.payload.data().restaurantId).subscribe((restaurantData: any) => {
          this.bookingService.getBookingByTurnId(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((bookingSnapShot) => {
            this.numberPersonTotal = 0;
            bookingSnapShot.forEach((bookingData: any) => {
              this.numberPersonTotal += bookingData.payload.doc.data().numberPerson;
            });
            this.turnId = this.activatedRoute.snapshot.paramMap.get('id');
            this.turn = turnData.payload.data();
            this.date = this.utility.dateFormat(turnData.payload.data().date);
            this.hour = this.utility.timeFormat(turnData.payload.data().date);
            this.restaurantId = turnData.payload.data().restaurantId;
            this.restaurant = restaurantData.payload.data();
            this.diners = turnData.payload.data().diners;
          });
        });
      });
    this.customerService.getCustomerByUid(this.authService.userData.uid).subscribe((customerSnapShot: any) => {
        this.customers = [];
        customerSnapShot.forEach((customerData: any) => {
          this.customers.push({
            id: customerData.payload.doc.id,
            data: customerData.payload.doc.data()
          });
        });
    });
  }

  ngOnInit() {
    this.bookingForm = this.fb.group({
      create: this.today,
      numberPerson: new FormControl('', Validators.compose([
        Validators.maxLength(80),
        Validators.required,
      ])),
      restaurantId: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      customerId: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      turnId: new FormControl('', Validators.compose([
        Validators.required
      ])),
    });
  }
  formSubmit() {
    if (!this.bookingForm.valid) {
      return false;
    } else {
      this.bookingService.createBooking(this.bookingForm.value).then(res => {
        console.log(res);
        this.bookingForm.reset();
        this.router.navigate(['/listBooking']);
      })
        .catch(error => console.log(error));
    }
  }
  maximo(numberPersonTotal: number, diners: number): number{
    return (diners - numberPersonTotal);
  }


}
