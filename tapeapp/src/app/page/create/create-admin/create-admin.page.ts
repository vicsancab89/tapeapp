import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from '../../../services/admin/admin.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.page.html',
  styleUrls: ['./create-admin.page.scss'],
})
export class CreateAdminPage implements OnInit {
  adminForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  validationMessages = {
    name: [
      { type: 'required', message: 'Name is required.' }
    ],
    surname: [
      { type: 'required', message: 'Surname is required.' }
    ],
    card: [
      { type: 'required', message: 'DNI is required.' },
      { type: 'pattern', message: 'DNI must be example 12345678X'}
    ],
    phone:[
      { type: 'required', message: 'Phone is required'},
      { type: 'pattern', message: 'Phone must be valid'}
    ],
    adress:[
      {type: 'required', message: 'Adress is required'}
    ],
    location:[
      {type: 'required', message: 'Location is required'}
    ],
  };
  constructor(
    private  adminService: AdminService,
    private router: Router,
    public authService: AuthenticationService,
    public fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.adminForm = this.fb.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      surname: new FormControl('', Validators.compose([
        Validators.required
      ])),
      card: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{8}[A-Z]{1}')
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9}')
      ])),
      adress: new FormControl('', Validators.compose([
        Validators.required
      ])),
      location: new FormControl('', Validators.compose([
        Validators.required
      ])),
      uid: this.authService.userData.uid,
    });
  }
  formSubmit() {
    if (!this.adminForm.valid) {
      return false;
    } else {
      this.adminService.createAdmin(this.adminForm.value).then(res => {
        console.log(res);
        this.adminForm.reset();
        this.authService.sendVerificationMail('admin');
        window.alert('Send verification email');
        this.authService.userData = this.authService.userLogado;
        this.router.navigate(['/listAdmin']);
      })
        .catch(error => console.log(error));
    }
  }
}
