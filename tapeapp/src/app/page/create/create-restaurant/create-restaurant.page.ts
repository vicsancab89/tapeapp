import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { RestaurantService } from '../../../services/restaurant.service';

@Component({
  selector: 'app-create-restaurant',
  templateUrl: './create-restaurant.page.html',
  styleUrls: ['./create-restaurant.page.scss'],
})
export class CreateRestaurantPage implements OnInit {
  restaurantForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  defaultDate = '1987-06-30';
  validationMessages = {
    name: [
      { type: 'required', message: 'Name is required.' },
    ],
    surname: [
      { type: 'required', message: 'Surname is required.' },
      { type: 'maxlenght', message: 'Surname max lenght is 80 character'}
    ],
    card: [
      { type: 'required', message: 'CIF is required.' },
      { type: 'pattern', message: 'CIF must be example X12345678'}
    ],
    phone:[
      { type: 'required', message: 'Phone is required'},
      { type: 'pattern', message: 'Phone must be valid'}
    ],
    adress:[
      {type: 'required', message: 'Adress is required'}
    ],
    location:[
      {type: 'required', message: 'Location is required'}
    ],
    capacity:[
      {type: 'required', message: 'Capacity is required'}
    ],
  };
  constructor(
    private  restaurantService: RestaurantService,
    private router: Router,
    public authService: AuthenticationService,
    public fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.restaurantForm = this.fb.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      surname: new FormControl('', Validators.compose([
        Validators.maxLength(80),
        Validators.required,
      ])),
      card: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[A-X]{1}[0-9]{8}')
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9}')
      ])),
      adress: new FormControl('', Validators.compose([
        Validators.required
      ])),
      location: new FormControl('', Validators.compose([
        Validators.required
      ])),
      capacity: new FormControl('', Validators.compose([
        Validators.required
      ])),
      uid: this.authService.userData.uid,
    });
  }
  formSubmit() {
    if (!this.restaurantForm.valid) {
      return false;
    } else {
      this.restaurantService.createRestaurant(this.restaurantForm.value).then(res => {
        console.log(res);
        this.restaurantForm.reset();
        this.authService.sendVerificationMail('restaurant');
        window.alert('Send verification email');
        this.authService.userData = this.authService.userLogado;
        this.router.navigate(['/listRestaurant']);
      })
        .catch(error => console.log(error));
    }
  }
}
