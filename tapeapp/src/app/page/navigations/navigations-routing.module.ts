import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NavigationsPage } from './navigations.page';

const routes: Routes = [
  {
    path: '',
    component: NavigationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NavigationsPageRoutingModule {}
