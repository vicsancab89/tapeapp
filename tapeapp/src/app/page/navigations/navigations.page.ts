import { Component, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { NavigationsService } from '../../services/navigation.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-navigations',
  templateUrl: './navigations.page.html',
  styleUrls: ['./navigations.page.scss'],
})
export class NavigationsPage implements OnInit {
  navigations: any[] = [];
  userId: any = this.authService.userData.uid;
  isActive: any = this.authService.userData.isActive;
  user: any = {};
  menu: any = {};
  constructor(
    public navigationService: NavigationsService,
    public authService: AuthenticationService,
    public userService: UserService,
    public router: Router
  ) {}

  ngOnInit() {
    console.log(this.userId);
    this.userService.getUser(this.userId).subscribe((userData: any) => {
      this.user = userData.payload.data();
      if(!userData.payload.data().isActive){
        this.authService.signOut();
      }
      switch (this.user.roleId) {
        case 'admin':
          this.navigationService
            .allNavigation()
            .subscribe((navigationSnapshot) => {
              this.navigations = [];
              navigationSnapshot.forEach((navigationData: any) => {
                this.menu = {};
                this.menu = navigationData.payload.doc.data();
                if (this.menu.isAdmin) {
                  this.navigations.push({
                    data: navigationData.payload.doc.data(),
                  });
                }
              });
            });
          break;
        case 'customer':
          this.navigationService
            .allNavigation()
            .subscribe((navigationSnapshot) => {
              this.navigations = [];
              navigationSnapshot.forEach((navigationData: any) => {
                this.menu = {};
                this.menu = navigationData.payload.doc.data();
                if (this.menu.isCustomer) {
                  this.navigations.push({
                    data: navigationData.payload.doc.data(),
                  });
                }
              });
            });
          break;
        case 'restaurant':
          this.navigationService
            .allNavigation()
            .subscribe((navigationSnapshot) => {
              this.navigations = [];
              navigationSnapshot.forEach((navigationData: any) => {
                this.menu = {};
                this.menu = navigationData.payload.doc.data();
                if (this.menu.isRestaurant) {
                  this.navigations.push({
                    data: navigationData.payload.doc.data(),
                  });
                }
              });
            });
          break;
        case 'staff':
          this.navigationService
            .allNavigation()
            .subscribe((navigationSnapshot) => {
              this.navigations = [];
              navigationSnapshot.forEach((navigationData: any) => {
                this.menu = {};
                this.menu = navigationData.payload.doc.data();
                if (this.menu.isStaff) {
                  this.navigations.push({
                    data: navigationData.payload.doc.data(),
                  });
                }
              });
            });
          break;
      }
    });
  }
  order(navigations: any): any{
    return navigations.sort((a, b) => a.data.priority - b.data.priority);
  }
}
