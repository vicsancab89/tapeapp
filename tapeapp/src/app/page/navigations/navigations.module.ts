import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NavigationsPageRoutingModule } from './navigations-routing.module';

import { NavigationsPage } from './navigations.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NavigationsPageRoutingModule
  ],
  declarations: [NavigationsPage]
})
export class NavigationsPageModule {}
