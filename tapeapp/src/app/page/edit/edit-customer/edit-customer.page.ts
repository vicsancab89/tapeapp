import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { CustomerService } from '../../../services/customer.service';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.page.html',
  styleUrls: ['./edit-customer.page.scss'],
})
export class EditCustomerPage implements OnInit {
  today: string = new Date(Date.now()).toISOString();
  customer: any = {};
  customerEditForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  defaultDate = '1987-06-30';
  validationMessages = {
    name: [
      { type: 'required', message: 'Name is required.' },
      { type: 'maxlenght', message: 'Name max lenght is 20 character'}
    ],
    surname: [
      { type: 'required', message: 'Surname is required.' },
      { type: 'maxlenght', message: 'Surname max lenght is 80 character'}
    ],
    card: [
      { type: 'required', message: 'DNI is required.' },
      { type: 'pattern', message: 'DNI must be example 12345678X'}
    ],
    phone:[
      { type: 'required', message: 'Phone is required'},
      { type: 'pattern', message: 'Phone must be valid'}
    ],
    adress:[
      {type: 'required', message: 'Adress is required'}
    ],
    location:[
      {type: 'required', message: 'Location is required'}
    ],
    birthday:[
      {type: 'required', message: 'Birthday is required'}
    ],
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private  customerService: CustomerService,
    private router: Router,
    public authService: AuthenticationService,
    public fb: FormBuilder,
  ) {
    this.customerService.getCustomer(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((adminData: any) => {
      this.customer = adminData.payload.data();
      this.customerEditForm = this.fb.group({
        name: new FormControl(this.customer.name, Validators.compose([
          Validators.maxLength(20),
          Validators.required
        ])),
        surname: new FormControl(this.customer.surname, Validators.compose([
          Validators.maxLength(80),
          Validators.required,
        ])),
        card: new FormControl(this.customer.card, Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{8}[A-Z]{1}')
        ])),
        phone: new FormControl(this.customer.phone, Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{9}')
        ])),
        adress: new FormControl(this.customer.adress, Validators.compose([
          Validators.required
        ])),
        location: new FormControl(this.customer.location, Validators.compose([
          Validators.required
        ])),
        birthday: new FormControl(this.customer.birthday, Validators.compose([
          Validators.required
        ])),
        uid: new FormControl(this.customer.uid),
      });
    });

  }

  ngOnInit() {
    this.customerEditForm = this.fb.group({
      name: new FormControl(this.customer.name, Validators.compose([
        Validators.maxLength(20),
        Validators.required
      ])),
      surname: new FormControl(this.customer.surname, Validators.compose([
        Validators.maxLength(80),
        Validators.required,
      ])),
      card: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{8}[A-Z]{1}')
      ])),
      phone: new FormControl(this.customer.phone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9}')
      ])),
      adress: new FormControl(this.customer.adress, Validators.compose([
        Validators.required
      ])),
      location: new FormControl(this.customer.location, Validators.compose([
        Validators.required
      ])),
      birthday: new FormControl(this.customer.birthday, Validators.compose([
        Validators.required
      ])),
      uid: new FormControl(this.authService.userData.uid),
    });
  }
  formUpdate() {
    if (!this.customerEditForm.valid) {
      return false;
    } else {
      this.customerService.updateCustomer(this.activatedRoute.snapshot.paramMap.get('id'), this.customerEditForm.value).then(res => {
        console.log(res);
        this.customerEditForm.reset();
        this.router.navigate(['/listCustomer']);
      })
        .catch(error => console.log(error));
    }
  }

}
