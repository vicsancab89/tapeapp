import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { BookingService } from '../../../services/booking.service';
import { CustomerService } from '../../../services/customer.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { TurnService } from '../../../services/turn.service';
import { UtilityService } from '../../../services/utility.service';

@Component({
  selector: 'app-edit-booking',
  templateUrl: './edit-booking.page.html',
  styleUrls: ['./edit-booking.page.scss'],
})
export class EditBookingPage implements OnInit {
  today: string = new Date(Date.now()).toISOString();
  numberPerson = 0;
  numberPersonTotal = 0;
  bookingId = '';
  booking1: any = {};
  booking: any = {};
  customerId = '';
  customer: any = {};
  turnId = '';
  turn: any = {};
  date= '';
  hour= '';
  diners = 0;
  restaurantId = '';
  restaurant: any = {};
  customers: any[] = [];
  bookingEditForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  validationMessages = {
    numberPerson: [
      { type: 'required', message: 'Number person is required.' }
    ],
    restaurantId: [
      { type: 'required', message: 'Restaurant is required.' },
    ],
    customerId:[
      { type: 'required', message: 'Customer is required'},
    ],
    turnId:[
      {type: 'required', message: 'Adress is required'}
    ],
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private turnService: TurnService,
    private customerService: CustomerService,
    private restaurantService: RestaurantService,
    private bookingService: BookingService,
    private fb: FormBuilder,
    private router: Router,
    private utility: UtilityService
  )
  {
    this.bookingService.getBooking(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((bookingData: any) => {
      this.restaurantService.getRestaurant(bookingData.payload.data().restaurantId).subscribe((restaurantData: any) => {
        this.turnService.getTurn(bookingData.payload.data().turnId).subscribe((turnData: any) =>{
          this.customerService.getCustomer(bookingData.payload.data().customerId).subscribe((customerData): any => {
            this.bookingId = this.activatedRoute.snapshot.paramMap.get('id');
            this.booking = bookingData.payload.data();
            this.numberPerson = bookingData.payload.data().numberPerson;
            this.customerId = bookingData.payload.data().customerId;
            this.customer = customerData.payload.data();
            this.date = this.utility.dateFormat(turnData.payload.data().date);
            this.hour = this.utility.timeFormat(turnData.payload.data().date);
            this.diners = turnData.payload.data().diners;
            this.turnId = bookingData.payload.data().turnId;
            this.turn = turnData.payload.data();
            this.restaurantId = bookingData.payload.data().restaurantId;
            this.restaurant = restaurantData.payload.data();
            this.bookingService.getBookingByTurnId(bookingData.payload.data().turnId).subscribe((bookingSnapShot2) => {
            this.numberPersonTotal = 0;
            bookingSnapShot2.forEach((bookingData2: any) => {
              this.numberPersonTotal += bookingData2.payload.doc.data().numberPerson;
            });
          });
          });
        });
      });
    });
    this.bookingService.getBooking(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((bookingData1: any) => {
      this.booking = bookingData1.payload.data();
      this.bookingEditForm = this.fb.group({
        create: this.today,
        numberPerson: new FormControl(this.booking1.numberPersonTotal, Validators.compose([
          Validators.required,
        ])),
        restaurantId: new FormControl(this.booking1.restaurantId, Validators.compose([
          Validators.required,
        ])),
        customerId: new FormControl(this.booking1.customerId, Validators.compose([
          Validators.required,
        ])),
        turnId: new FormControl(this.booking1.turnId, Validators.compose([
          Validators.required
        ])),
      });
    });
  }

  ngOnInit() {
    this.bookingEditForm = this.fb.group({
      create: this.today,
      numberPerson: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      restaurantId: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      customerId: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      turnId: new FormControl('', Validators.compose([
        Validators.required
      ])),
    });
  }
  formUpdate() {
    if (!this.bookingEditForm.valid) {
      return false;
    } else {
      this.bookingService.updateBooking(this.activatedRoute.snapshot.paramMap.get('id'), this.bookingEditForm.value).then(res => {
        console.log(res);
        this.bookingEditForm.reset();
        this.router.navigate(['/listBooking']);
      })
        .catch(error => console.log(error));
    }
  }
  maximo(numberPersonTotal: number, diners: number, numberPerson: number): number{
    return ((diners - numberPersonTotal) + numberPerson);
  }

}
