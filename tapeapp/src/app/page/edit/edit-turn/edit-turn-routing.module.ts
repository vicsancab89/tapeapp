import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditTurnPage } from './edit-turn.page';

const routes: Routes = [
  {
    path: '',
    component: EditTurnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditTurnPageRoutingModule {}
