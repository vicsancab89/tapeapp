import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { TurnService } from '../../../services/turn.service';

@Component({
  selector: 'app-edit-turn',
  templateUrl: './edit-turn.page.html',
  styleUrls: ['./edit-turn.page.scss'],
})
export class EditTurnPage implements OnInit {
  today: string = new Date(Date.now()).toISOString();
  turn: any = {};
  turnEditForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  restaurants: any [] = [];
  validationMessages = {
    date: [{ type: 'required', message: 'Date is required.' }],
    diners: [
      {type: 'required', message: 'Diners is required'}
    ],
    restaurantId: [{type: 'required', message: 'Restaurant is required'}]
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private restaurantService: RestaurantService,
    private turnService: TurnService,
    public authService: AuthenticationService,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.restaurantService.getRestaurantByUid(this.authService.userExist.uid).subscribe((restaurantSnapShot: any) => {
      restaurantSnapShot.forEach((restaurantData: any) => {
         this.restaurants = [];
         this.restaurants.push({
            id: restaurantData.payload.doc.id,
            data: restaurantData.payload.doc.data()
         });
      });
    });
    this.turnService.getTurn(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((turnData: any) => {
      this.turn = turnData.payload.data();
      this.turnEditForm = this.fb.group({
        date: new FormControl(this.turn.date, Validators.compose([Validators.required])),
        diners: new FormControl(this.turn.diners),
        restaurantId: new FormControl(this.turn.restaurantId, Validators.compose([Validators.required])),
      });
    });
   }

  ngOnInit() {
    this.turnEditForm = this.fb.group({
      date: new FormControl('', Validators.compose([Validators.required])),
      diners: new FormControl(''),
      restaurantId: new FormControl('', Validators.compose([Validators.required])),
    });
  }
  formUpdate() {
    if (!this.turnEditForm.valid) {
      return false;
    } else {
      this.turnService.updateTurn(this.activatedRoute.snapshot.paramMap.get('id'), this.turnEditForm.value).then(res => {
        console.log(res);
        this.turnEditForm.reset();
        this.router.navigate(['/listTurn']);
      })
        .catch(error => console.log(error));
    }
  }
}
