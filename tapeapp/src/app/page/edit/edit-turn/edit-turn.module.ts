import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditTurnPageRoutingModule } from './edit-turn-routing.module';

import { EditTurnPage } from './edit-turn.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EditTurnPageRoutingModule
  ],
  declarations: [EditTurnPage]
})
export class EditTurnPageModule {}
