import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from '../../../services/admin/admin.service';

@Component({
  selector: 'app-edit-admin',
  templateUrl: './edit-admin.page.html',
  styleUrls: ['./edit-admin.page.scss'],
})
export class EditAdminPage implements OnInit {
  admin: any = {};
  adminEditForm: FormGroup = new FormGroup({});
  idAdmin: any;
  errorMessage: string;
  successMessage: string;
  validationMessages = {
    name: [
      { type: 'required', message: 'Name is required.' }
    ],
    surname: [
      { type: 'required', message: 'Surname is required.' }
    ],
    card: [
      { type: 'required', message: 'DNI is required.' },
      { type: 'pattern', message: 'DNI must be example 12345678X'}
    ],
    phone:[
      { type: 'required', message: 'Phone is required'},
      { type: 'pattern', message: 'Phone must be valid'}
    ],
    adress:[
      {type: 'required', message: 'Adress is required'}
    ],
    location:[
      {type: 'required', message: 'Location is required'}
    ],
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private adminService: AdminService,
    private router: Router,
    public fb: FormBuilder
  )
  {
    this.adminService.getAdmin(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((adminData: any) => {
      this.admin = adminData.payload.data();
      this.adminEditForm = this.fb.group({
        name: new FormControl(this.admin.name, Validators.compose([
        Validators.required
      ])),
      surname: new FormControl(this.admin.surname, Validators.compose([
        Validators.required
      ])),
      card: new FormControl(this.admin.card, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{8}[A-Z]{1}')
      ])),
      phone: new FormControl(this.admin.phone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9}')
      ])),
      adress: new FormControl(this.admin.adress, Validators.compose([
        Validators.required
      ])),
      location: new FormControl(this.admin.location, Validators.compose([
        Validators.required
      ])),
      uid: new FormControl(this.admin.uid)
      });
    });
  }

  ngOnInit() {

      this.adminEditForm = this.fb.group({
        name: new FormControl('', Validators.compose([
          Validators.required
        ])),
        surname: new FormControl('', Validators.compose([
          Validators.required
        ])),
        card: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{8}[A-Z]{1}')
        ])),
        phone: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{9}')
        ])),
        adress: new FormControl('', Validators.compose([
          Validators.required
        ])),
        location: new FormControl('', Validators.compose([
          Validators.required
        ])),
        uid: ('')
      });
  }
  formUpdate() {
    if (!this.adminEditForm.valid) {
      return false;
    } else {
      this.adminService.updateAdmin(this.activatedRoute.snapshot.paramMap.get('id'), this.adminEditForm.value).then(res => {
        console.log(res);
        this.adminEditForm.reset();
        this.router.navigate(['/listAdmin']);
      })
        .catch(error => console.log(error));
    }
  }
}


