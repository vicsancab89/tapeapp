import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditStaffPageRoutingModule } from './edit-staff-routing.module';

import { EditStaffPage } from './edit-staff.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EditStaffPageRoutingModule
  ],
  declarations: [EditStaffPage]
})
export class EditStaffPageModule {}
