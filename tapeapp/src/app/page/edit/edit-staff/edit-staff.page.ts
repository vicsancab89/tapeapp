import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { StaffService } from '../../../services/staff.service';

@Component({
  selector: 'app-edit-staff',
  templateUrl: './edit-staff.page.html',
  styleUrls: ['./edit-staff.page.scss'],
})
export class EditStaffPage implements OnInit {
  staff: any = {};
  restaurants: any[] = [];
  staffEditForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  validationMessages = {
    position: [
      { type: 'required', message: 'Name is required.' }
    ],
    name: [
      { type: 'required', message: 'Name is required.' }
    ],
    contract:[
      {type: 'required', message: 'Location is required'}
    ],
    surname: [
      { type: 'required', message: 'Surname is required.' }
    ],
    card: [
      { type: 'required', message: 'DNI is required.' },
      { type: 'pattern', message: 'DNI must be example 12345678X'}
    ],
    phone:[
      { type: 'required', message: 'Phone is required'},
      { type: 'pattern', message: 'Phone must be valid'}
    ],
    adress:[
      {type: 'required', message: 'Adress is required'}
    ],
    location:[
      {type: 'required', message: 'Location is required'}
    ],
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private  staffService: StaffService,
    private restaurantService: RestaurantService,
    private router: Router,
    public authService: AuthenticationService,
    public fb: FormBuilder,
  ) {
    this.restaurantService.allRestaurant().subscribe((restaurantSnapshot) => {
      this.restaurants = [];
      restaurantSnapshot.forEach((restaurantData: any) => {
        console.log(this.authService.userExist.roleId);
        if (this.authService.userExist.roleId === 'admin') {
          this.restaurants.push({
            id: restaurantData.payload.doc.id,
            data: restaurantData.payload.doc.data(),
          });
        } else if (this.authService.userExist.roleId === 'restaurant') {
          if (
            this.authService.userExist.uid ===
            restaurantData.payload.doc.data().uid
          ) {
            this.restaurants.push({
              id: restaurantData.payload.doc.id,
              data: restaurantData.payload.doc.data(),
            });
          }
        }
      });
    });
    this.staffService.getStaff(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((staffData: any) => {
      this.staff = staffData.payload.data();
      this.staffEditForm = this.fb.group({
        position: new FormControl(this.staff.position, Validators.compose([
          Validators.required
        ])),
        name: new FormControl(this.staff.name, Validators.compose([
          Validators.required
        ])),
        surname: new FormControl(this.staff.surname, Validators.compose([
          Validators.required
        ])),
        card: new FormControl(this.staff.card, Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{8}[A-Z]{1}')
        ])),
        phone: new FormControl(this.staff.phone, Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{9}')
        ])),
        adress: new FormControl(this.staff.adress, Validators.compose([
          Validators.required
        ])),
        location: new FormControl(this.staff.location, Validators.compose([
          Validators.required
        ])),
        contract: new FormControl(this.staff.contract, Validators.compose([Validators.required])),
        uid: new FormControl(this.authService.userData.uid)
      });
    });
   }

  ngOnInit() {
    this.staffEditForm = this.fb.group({
      position: new FormControl('', Validators.compose([
        Validators.required
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      surname: new FormControl('', Validators.compose([
        Validators.required
      ])),
      card: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{8}[A-Z]{1}')
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9}')
      ])),
      adress: new FormControl('', Validators.compose([
        Validators.required
      ])),
      location: new FormControl('', Validators.compose([
        Validators.required
      ])),
      contract: new FormControl('', Validators.compose([Validators.required])),
      uid: ('')
    });
  }
  formUpdate() {
    if (!this.staffEditForm.valid) {
      return false;
    } else {
      this.staffService.updateStaff(this.activatedRoute.snapshot.paramMap.get('id'), this.staffEditForm.value).then(res => {
        console.log(res);
        this.staffEditForm.reset();
        this.router.navigate(['/listStaff']);
      })
        .catch(error => console.log(error));
    }
  }
}
