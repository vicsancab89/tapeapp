import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { RestaurantService } from '../../../services/restaurant.service';

@Component({
  selector: 'app-edit-restaurant',
  templateUrl: './edit-restaurant.page.html',
  styleUrls: ['./edit-restaurant.page.scss'],
})
export class EditRestaurantPage implements OnInit {
  restaurant: any = {};
  restaurantEditForm: FormGroup;
  errorMessage: string;
  successMessage: string;
  defaultDate = '1987-06-30';
  validationMessages = {
    name: [
      { type: 'required', message: 'Name is required.' },
    ],
    surname: [
      { type: 'required', message: 'Surname is required.' },
      { type: 'maxlenght', message: 'Surname max lenght is 80 character'}
    ],
    card: [
      { type: 'required', message: 'CIF is required.' },
      { type: 'pattern', message: 'CIF must be example X12345678'}
    ],
    phone:[
      { type: 'required', message: 'Phone is required'},
      { type: 'pattern', message: 'Phone must be valid'}
    ],
    adress:[
      {type: 'required', message: 'Adress is required'}
    ],
    location:[
      {type: 'required', message: 'Location is required'}
    ],
    capacity:[
      {type: 'required', message: 'Capacity is required'}
    ],
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private  restaurantService: RestaurantService,
    private router: Router,
    public authService: AuthenticationService,
    public fb: FormBuilder,
  )
  {
    this.restaurantService.getRestaurant(this.activatedRoute.snapshot.paramMap.get('id')).subscribe((restaurantData: any) => {
      this.restaurant = restaurantData.payload.data();
      this.restaurantEditForm = this.fb.group({
        name: new FormControl(this.restaurant.name, Validators.compose([
          Validators.required
        ])),
        surname: new FormControl(this.restaurant.surname, Validators.compose([
          Validators.maxLength(80),
          Validators.required,
        ])),
        card: new FormControl(this.restaurant.card, Validators.compose([
          Validators.required,
          Validators.pattern('[A-X]{1}[0-9]{8}')
        ])),
        phone: new FormControl(this.restaurant.phone, Validators.compose([
          Validators.required,
          Validators.pattern('[0-9]{9}')
        ])),
        adress: new FormControl(this.restaurant.adress, Validators.compose([
          Validators.required
        ])),
        location: new FormControl(this.restaurant.location, Validators.compose([
          Validators.required
        ])),
        capacity: new FormControl(this.restaurant.capacity, Validators.compose([
          Validators.required
        ])),
        uid: new FormControl(this.restaurant.uid)
      });
    });
   }
  ngOnInit()
  {
    this.restaurantEditForm = this.fb.group({
      name: new FormControl('', Validators.compose([
        Validators.required
      ])),
      surname: new FormControl('', Validators.compose([
        Validators.maxLength(80),
        Validators.required,
      ])),
      card: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[A-X]{1}[0-9]{8}')
      ])),
      phone: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9}')
      ])),
      adress: new FormControl('', Validators.compose([
        Validators.required
      ])),
      location: new FormControl('', Validators.compose([
        Validators.required
      ])),
      capacity: new FormControl('', Validators.compose([
        Validators.required
      ])),
      uid: new FormControl('')
    });
  }
  formUpdate() {
    if (!this.restaurantEditForm.valid) {
      return false;
    } else {
      this.restaurantService.updateRestaurant(this.activatedRoute.snapshot.paramMap.get('id'), this.restaurantEditForm.value).then(res => {
        console.log(res);
        this.restaurantEditForm.reset();
        this.router.navigate(['/listRestaurant']);
      })
        .catch(error => console.log(error));
    }
  }
}
