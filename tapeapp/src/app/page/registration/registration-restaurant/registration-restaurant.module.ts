import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrationRestaurantPageRoutingModule } from './registration-restaurant-routing.module';

import { RegistrationRestaurantPage } from './registration-restaurant.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RegistrationRestaurantPageRoutingModule
  ],
  declarations: [RegistrationRestaurantPage]
})
export class RegistrationRestaurantPageModule {}
