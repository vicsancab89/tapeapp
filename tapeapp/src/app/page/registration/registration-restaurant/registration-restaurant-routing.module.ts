import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrationRestaurantPage } from './registration-restaurant.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrationRestaurantPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrationRestaurantPageRoutingModule {}
