import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrationStaffPage } from './registration-staff.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrationStaffPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrationStaffPageRoutingModule {}
