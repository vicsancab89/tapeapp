import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrationStaffPageRoutingModule } from './registration-staff-routing.module';

import { RegistrationStaffPage } from './registration-staff.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RegistrationStaffPageRoutingModule
  ],
  declarations: [RegistrationStaffPage]
})
export class RegistrationStaffPageModule {}
