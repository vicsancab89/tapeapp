import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistrationCustomerPageRoutingModule } from './registration-customer-routing.module';

import { RegistrationCustomerPage } from './registration-customer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RegistrationCustomerPageRoutingModule
  ],
  declarations: [RegistrationCustomerPage]
})
export class RegistrationCustomerPageModule {}
