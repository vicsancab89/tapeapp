import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrationCustomerPage } from './registration-customer.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrationCustomerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistrationCustomerPageRoutingModule {}
