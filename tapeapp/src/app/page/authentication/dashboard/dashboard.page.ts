import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../../services/admin/admin.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { CustomerService } from '../../../services/customer.service';
import { RestaurantService } from '../../../services/restaurant.service';
import { StaffService } from '../../../services/staff.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  people: any[] = [];
  uId: any = this.authService.userLogado.uid;
  public userAccount: any = {};
  person: any ={};
  constructor(
    private userService: UserService,
    public authService: AuthenticationService,
    private adminService: AdminService,
    private restaurantService: RestaurantService,
    private staffService: StaffService,
    private customerService: CustomerService) {}
  ngOnInit() {
    this.userService.getUser(this.uId).subscribe((userData: any)=>{
      this.userAccount = userData.payload.data();
      console.log(this.userAccount.roleId);
      switch (userData.payload.data().roleId) {
        case 'admin':
          this.adminService.getAdminByUid(this.uId).subscribe((adminSnapsShot: any)=>{
            this.people = [];
            adminSnapsShot.forEach((adminData: any) =>{
              this.people.push({
              data: adminData.payload.doc.data()
              });
            });
          });
        break;
        case 'customer':
          this.customerService.getCustomerByUid(this.uId).subscribe((customerSnapsShot: any)=>{
            this.people = [];
            customerSnapsShot.forEach((customerData: any) =>{
              this.people.push({
              data: customerData.payload.doc.data()
              });
            });
          });
        break;
        case 'restaurant':
          this.restaurantService.getRestaurantByUid(this.uId).subscribe((restaurantSnapsShot: any)=>{
            this.people = [];
            restaurantSnapsShot.forEach((restaurantData: any) =>{
              this.people.push({
              data: restaurantData.payload.doc.data()
              });
            });
          });
        break;
        case 'staff':
          this.staffService.getStaffByUid(this.uId).subscribe((staffSnapsShot: any)=>{
            this.people = [];
            staffSnapsShot.forEach((staffData: any) =>{
              this.people.push({
              data: staffData.payload.doc.data()
              });
            });
          });
        break;

      }
    });
  }
}
