export abstract class Person{
  name: string;
  surname: string;
  card: string;
  phone: string;
  adress: string;
  location: string;
  uid: string;
}
