export class Booking{
  create: string;
  numberPerson: string;
  restaurantId: string;
  customerId: string;
  turnId: string;
}
