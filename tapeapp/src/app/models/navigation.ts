export class Navigation{
  isAdmin: boolean;
  isCustomer: boolean;
  isRestaurant: boolean;
  isStaff: boolean;
  name: string;
  route: string;
}
