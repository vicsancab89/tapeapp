export class User {
  uid: string;
  email: string;
  roleId: string;
  isActive: boolean;
}
