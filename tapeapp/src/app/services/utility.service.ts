import { Injectable } from '@angular/core';
import { BookingService } from './booking.service';
import { TurnService } from './turn.service';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(
    private bookingService: BookingService,
    private turnService: TurnService
    ) { }
  dateFormat(iso8601: string ): string{
    const date = new Date(iso8601);
    const months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    const formattedDate = date.getDate() + '-' + months[date.getMonth()] + '-' + date.getFullYear();
    return formattedDate;
    }
    timeFormat(iso8601: string): string{
      const date = new Date(iso8601);
      return date.toLocaleTimeString();
    }
    compareDate(iso8601: string): boolean{
      const date = new Date(iso8601);
      const now = new Date(Date.now());
      return date >= now;
    }
    morePersonThatBooking(turnId: string): boolean{
      let numberPersonTotal = 0;
      this.turnService.getTurn(turnId).subscribe((turnData: any) => {
        this.bookingService.getBookingByTurnId(turnId).subscribe((bookingSnapShot) =>{
          bookingSnapShot.forEach((bookingData: any) => {
            numberPersonTotal += bookingData.payload.doc.data().numberPerson;
            if(numberPersonTotal > turnData.payload.doc.data().diners){
              return false;
            }
          });
        });
      });
      return true;
    }
}
