import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Turn } from '../models/turn';

@Injectable({
  providedIn: 'root'
})
export class TurnService {

  constructor(private firestore: AngularFirestore) { }
  createTurn(turn: Turn){
    return this.firestore.collection('turns').add(turn);
  }
  getTurn(id: string){
    return this.firestore.collection('turns').doc(id).snapshotChanges();
  }
  allTurn() {
    return this.firestore.collection('turns').snapshotChanges();
  }
  updateTurn(id, turn: Turn) {
    return this.firestore.collection('turns').doc(id).set(turn);
  }
  deleteTurn(id: string) {
    return this.firestore.collection('turns').doc(id).delete();
  }
  getTurnByRestaurantId(restaurantId: string){
    return this.firestore.collection('turns', ref => ref.where('restaurantId','==', restaurantId)).snapshotChanges();
  }
}
