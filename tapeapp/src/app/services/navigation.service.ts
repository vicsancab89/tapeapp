import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Navigation } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavigationsService {

  constructor(private firestore: AngularFirestore) { }
  createNavigation(navigation: Navigation){
    return this.firestore.collection('navigations').add(navigation);
  }
  getNavigation(id: string){
    return this.firestore.collection('navigations').doc(id).snapshotChanges();
  }
  allNavigation() {
    return this.firestore.collection('navigations').snapshotChanges();
  }
  updateNavigation(id, navigation: Navigation) {
    return this.firestore.collection('navigations').doc(id).set(navigation);
  }
  deleteCustomer(id: string) {
    return this.firestore.collection('navigations').doc(id).delete();
  }
}
