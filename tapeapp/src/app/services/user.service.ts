import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private firestore: AngularFirestore)
  {}
  createUser(user: User){
    return this.firestore.collection('users').add(user);
  }
  getUser(id: string){
    return this.firestore.collection('users').doc(id).snapshotChanges();
  }
  rolUser(uid: string){
    return this.firestore.collection('users').doc(uid).snapshotChanges();
  }
}
