import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Admin } from '../../models/admin';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private firestore: AngularFirestore)
  {}
  createAdmin(admin: Admin){
    return this.firestore.collection('admins').add(admin);
  }
  getAdmin(id: string){
    return this.firestore.collection('admins').doc(id).snapshotChanges();
  }
  allAdmin() {
    return this.firestore.collection('admins').snapshotChanges();
  }
  updateAdmin(id, admin: any) {
    return this.firestore.collection('admins').doc(id).set(admin);
  }
  deleteAdmin(id: string) {
    return this.firestore.collection('admins').doc(id).delete();
  }
  getAdminByUid(uid: string){
    return this.firestore.collection('admins', ref => ref.where('uid','==', uid)).snapshotChanges();
  }
}
