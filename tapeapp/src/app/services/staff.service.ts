import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Staff } from '../models/staff';

@Injectable({
  providedIn: 'root'
})
export class StaffService {

  constructor(private firestore: AngularFirestore)
  {}
  createStaff(staff: Staff){
    return this.firestore.collection('staffs').add(staff);
  }
  getStaff(id: string){
    return this.firestore.collection('staffs').doc(id).snapshotChanges();
  }
  allStaff() {
    return this.firestore.collection('staffs').snapshotChanges();
  }
  updateStaff(id, staff: Staff) {
    return this.firestore.collection('staffs').doc(id).set(staff);
  }
  deleteStaff(id: string) {
    return this.firestore.collection('staffs').doc(id).delete();
  }
  getStaffByUid(uid: string){
    return this.firestore.collection('staffs', ref => ref.where('uid','==', uid)).snapshotChanges();
  }
  getStaffByContract(contract: string){
    return this.firestore.collection('staffs', ref => ref.where('contract','==', contract)).snapshotChanges();
  }
}
