import { TestBed } from '@angular/core/testing';

import { NavigationsService } from './navigation.service';

describe('NavigationService', () => {
  let service: NavigationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NavigationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
