import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Booking } from '../models/booking';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private firestore: AngularFirestore)
  {}
  createBooking(booking: Booking){
    return this.firestore.collection('bookings').add(booking);
  }
  getBooking(id: string){
    return this.firestore.collection('bookings').doc(id).snapshotChanges();
  }
  allBooking() {
    return this.firestore.collection('bookings').snapshotChanges();
  }
  updateBooking(id, booking: any) {
    return this.firestore.collection('bookings').doc(id).set(booking);
  }
  deleteBooking(id: string) {
    return this.firestore.collection('bookings').doc(id).delete();
  }
  getBookingByTurnId(turnId: string){
    return this.firestore.collection('bookings', ref => ref.where('turnId','==', turnId)).snapshotChanges();
  }
  getBookingByCustomerId(customerId: string){
    return this.firestore.collection('bookings', ref => ref.where('customerId','==', customerId)).snapshotChanges();
  }
  getBookingByRestaurantId(restaurantId: string){
    return this.firestore.collection('bookings', ref => ref.where('restaurantId','==', restaurantId)).snapshotChanges();
  }
}
