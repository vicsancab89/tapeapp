import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Check } from '../models/check';

@Injectable({
  providedIn: 'root'
})
export class CheckService {

  constructor(private firestore: AngularFirestore)
  {}
  createCheck(check: Check){
    return this.firestore.collection('checks').add(check);
  }
  getCheck(id: string){
    return this.firestore.collection('checks').doc(id).snapshotChanges();
  }
  allCheck() {
    return this.firestore.collection('checks').snapshotChanges();
  }
  updateCheck(id, check: Check) {
    return this.firestore.collection('checks').doc(id).set(check);
  }
  deleteCheck(id: string) {
    return this.firestore.collection('checks').doc(id).delete();
  }
  getCheckByStaffId(staffId: string){
    return this.firestore.collection('checks', ref => ref.where('staffId','==', staffId)).snapshotChanges();
  }
  getCheckByRestaurantId(restaurantId: string){
    return this.firestore.collection('checks', ref => ref.where('restaurantId','==', restaurantId)).snapshotChanges();
  }
}
