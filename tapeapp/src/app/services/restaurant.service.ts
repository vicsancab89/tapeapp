import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Restaurant } from '../models/restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private firestore: AngularFirestore)
  {}
  createRestaurant(restaurant: Restaurant){
    return this.firestore.collection('restaurants').add(restaurant);
  }
  getRestaurant(id: string){
    return this.firestore.collection('restaurants').doc(id).snapshotChanges();
  }
  allRestaurant() {
    return this.firestore.collection('restaurants').snapshotChanges();
  }
  updateRestaurant(id, restaurant: Restaurant) {
    return this.firestore.collection('restaurants').doc(id).set(restaurant);
  }
  deleteRestaurant(id: string) {
    return this.firestore.collection('restaurants').doc(id).delete();
  }
  getRestaurantByUid(uid: string){
    return this.firestore.collection('restaurants', ref => ref.where('uid','==', uid)).snapshotChanges();
  }
}
