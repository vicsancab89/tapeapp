import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Customer } from '../models/customer';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private firestore: AngularFirestore)
  {}
  createCustomer(customer: Customer){
    return this.firestore.collection('customers').add(customer);
  }
  getCustomer(id: string){
    return this.firestore.collection('customers').doc(id).snapshotChanges();
  }
  allCustomer() {
    return this.firestore.collection('customers').snapshotChanges();
  }
  updateCustomer(id, customer: Customer) {
    return this.firestore.collection('customers').doc(id).set(customer);
  }
  deleteCustomer(id: string) {
    return this.firestore.collection('customers').doc(id).delete();
  }
  getCustomerByUid(uid: string){
    return this.firestore.collection('customers', ref => ref.where('uid','==', uid)).snapshotChanges();
  }
}
