import { Injectable, NgZone } from '@angular/core';
import * as auth from 'firebase/auth';
import { User } from '../../models/user';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat';
import {AngularFirestore, AngularFirestoreDocument,} from '@angular/fire/compat/firestore';
import { UserService } from '../user.service';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  userData: any = {};
  userLogado: any ={};
  userExist: any = {};
  roleId = '';
  uid = '';
  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public userService: UserService,
    public router: Router,
    public ngZone: NgZone
  ){
    this.afAuth.setPersistence('session').then(() => {
      this.afAuth.authState.subscribe((user) => {
        if (user) {
          this.userData = user;
          localStorage.setItem('user', JSON.stringify(this.userData));
        } else {
          localStorage.setItem('user', 'null');
          JSON.parse(localStorage.getItem('user'));
        }
      });
    });
  }
   // Devuelve true cuando el usuario está logeado y el email verificado
   get isEmailVerified(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false);;
  }
  // Sign in with email/password
  signIn(email, password) {
   return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
          if(result.user.emailVerified){
              this.ngZone.run(() => {
                this.userData = this.getUserData(result.user);
                this.userLogado = this.getUserData(result.user);
                this.userService.getUser(result.user.uid).subscribe((userData: any) =>{
                  this.roleId = userData.payload.data().roleId;
                  this.uid =result.user.uid;
                });
                this.router.navigate(['navigations']);
            });
          }else {
            window.alert('Email is not verified');
            return false;
          }
      }).catch((error) => {
        window.alert(error.message);
      });
  }
  // Sign up with email/password
  signUp(email: any, password: any, role: any) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign
        up and returns promise */
        this.sendVerificationMail(role);
        this.setUserData(result.user, role, true);
      }).catch((error) => {
        window.alert(error.message);
      });
  }
  sendVerificationMail(role: any) {
    return this.afAuth.currentUser
      .then( u => u?.sendEmailVerification())
      .then(() => {
        switch (role) {
          case 'admin':
            this.router.navigate(['createAdmin']);
            break;
          case 'customer':
              this.router.navigate(['createCustomer']);
              break;
          case 'restaurant':
              this.router.navigate(['createRestaurant']);
              break;
          case 'staff':
            this.router.navigate(['createStaff']);
              break;
        }
      });
  }
  // Reset Forggot password
  forgotPassword(passwordResetEmail: string) {
    return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
        this.router.navigate(['home']);
      }).catch((error) => {
        window.alert(error);
      });
  }
  /* Setting up user data when sign in with username/password,
  sign up with username/password and sign in with social auth
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  setUserData(user: any, role: any, active: any) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userD: User = {
      uid: user.uid,
      email: user.email,
      roleId: role,
      isActive: active
    };
    return userRef.set(userD, {
      merge: true
    });
  }
  getUserData(user: any): any {
    const userToken: any = {
        uid: user.uid,
        email: user.email,
        emailVerified: user.emailVerified,
        roleId: this.userExist.roleId,
        isActive: this.userExist.isActive
      };
      return userToken;
  }
  signOut() {
   return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.userData = {};
      this.userExist = {};
      this.userLogado = {};
      this.roleId = '';
      this.uid = '';
      this.router.navigate(['home']);
    });
  }
  delete(user){
    this.setUserData(user, user.roleId, false);
  }


}
