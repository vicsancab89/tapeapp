// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAD98BC2wcbUxbNWwRQJQHieAKXpr1hNOA',
    authDomain: 'tapeapp-12c7a.firebaseapp.com',
    projectId: 'tapeapp-12c7a',
    storageBucket: 'tapeapp-12c7a.appspot.com',
    messagingSenderId: '1001882817103',
    appId: '1:1001882817103:web:ffe42a65c931bb2072503a',
    measurementId: 'G-FQ3NGZ11FP'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
